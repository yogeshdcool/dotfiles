call plug#begin('~/.nvim/config/plugged')

Plug 'mhinz/vim-startify'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'farmergreg/vim-lastplace'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'vimwiki/vimwiki'
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }
Plug 'tpope/vim-commentary'
Plug 'EvanQuan/vim-executioner'
Plug 'ryanoasis/vim-devicons'

call plug#end()

set mouse=a
set clipboard=unnamedplus
set noshowmode
set ignorecase
set smartcase
set number relativenumber
set incsearch                   " Incremental search
set hidden                      " Needed to keep multiple buffers open
set nobackup                    " No auto backups
set noswapfile                  " No swap

set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.

let mapleader=","

let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }
let g:limelight_conceal_ctermfg = 240

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
